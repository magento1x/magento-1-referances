# Magento 1 - Referances

List of the useful documents, extensions, companies that support still m1.

## Companies:

1- [Amasty](https://amasty.com/magento-extensions.html)

2- [Plumrocket](https://plumrocket.com/magento-1-extensions)

3- [Mirasvit](https://mirasvit.com/magento-extensions.html)

4- [Aheadworks](https://aheadworks.com/magento-1-extensions)

5- [Magecomp](https://magecomp.com/magento-extensions.html)

6- [Mageworx](https://www.mageworx.com/magento-extensions.html)

7- [Meetanshi](https://meetanshi.com/magento-extensions.html)

8- [Inchoo](https://inchoo.net/magento/)

9- [Flagbit](https://github.com/flagbit)

10- [Fishpig](https://fishpig.co.uk/magento/extensions/)

11- [magepal](https://github.com/magepal)



## Long Term Support

* [OpenMage](https://www.openmage.org/)


## Documents

* [cloudways](https://www.cloudways.com/blog/magento/magento-1/)

## Repos
